```plantuml

node Prometheus
node Alertmanager
node Grafana
node "OpenSearch Exporter"
node Email
node Webhook
Prometheus --|> Alertmanager : https
Alertmanager ~~|> Email
Alertmanager ~~|> Webhook
"OpenSearch Exporter" <|-- Prometheus : https/9200
Prometheus <|-- Grafana

```

|     | DEV    | PROD |
|-----------------|:-------------|:-------------|
| OpenShift URLs| https://console.cntcl.cmcdev.be:8443 | https://console.cntcl.cmc.be:8443  |
| Prometheus URLs|  | https://prometheus-infra-mon.apps.cntcl.cmc.be/ |
| Alertmanager URLs| | https://alertmanager-infra-mon.apps.cntcl.cmc.be/ |
| Grafana URLs|  | https://grafana-infra-mon.apps.cntcl.cmc.be/ |

#  Prometheus/Alertmanager and Grafana on Openshift Installation

Login to a server where the 'oc' binary is installed e.g. s51ld6y (jumphost)
```
$ ssh s51ld6y
$ cd /opt/ocp
```
copy login command from the Openshift GUI (upper right corner on your account )

```
oc login https://console.cntcl.cmc.be:8443 --token=4Sevi7HR6Nam6jfaKE4gHJu7V5wyUB1Ef2cW-dzh7HI
```
choose a project
```
oc project infra-mon
```

## Deploy Prometheus
Run the Prometheus deployment & configuration

**Note:** The Gitlab URL to the yaml file in using the API format. Also note the %2F in the subdirectory part! It's also best to first create the config files and then the actual deployment.

```
kubectl apply -f https://gitlab.com/xpericon/Evolane-demo/-/raw/main/prometheus/prometheus-configmaps.yaml
kubectl apply -f https://gitlab.com/xpericon/Evolane-demo/-/raw/main/prometheus/prometheus-deployment.yaml

```

## Deploy Grafana
Run the Grafana deployment & configuration
```
oc apply -f http://gitlab-151.cmcdev.be:81/api/v4/projects/5745/repository/files/grafana%2Fgrafana-configmaps.yaml/raw?private_token=glpat-jn7msRuLbqe_c2C3PnUF
oc apply -f http://gitlab-151.cmcdev.be:81/api/v4/projects/5745/repository/files/grafana%2Fgrafana-deployment.yaml/raw?private_token=glpat-jn7msRuLbqe_c2C3PnUF
```

### Deploy AlertManager
Run the AlertManager deployment & configuration
```
oc apply -f http://gitlab-151.cmcdev.be:81/api/v4/projects/5745/repository/files/alertmanager%2Falertmanager-deployment.yaml/raw?private_token=glpat-jn7msRuLbqe_c2C3PnUF
oc apply -f http://gitlab-151.cmcdev.be:81/api/v4/projects/5745/repository/files/alertmanager%2Falertmanager-configmaps.yaml/raw?private_token=glpat-jn7msRuLbqe_c2C3PnUF

```
